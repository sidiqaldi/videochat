#installation

1. run `composer update`
2. setup db in `.env` as per your system environtment
3. run `php artisan migrate`
4. run `php artisan serve` to work localy or just set `/public` path in web server to access it by port 80

#note
1. make sure using SSL for media access or the camera & microphone won't work